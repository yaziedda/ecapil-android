package id.gakendor.ecapil.android.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import id.gakendor.ecapil.android.R;

public class AppIntroActivity extends AppIntro2 {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SliderPage sliderPage1 = new SliderPage();
        sliderPage1.setTitle("Permohonan Online");
        sliderPage1.setDescription("Pengajuan permohonan capil secara onine");
        sliderPage1.setImageDrawable(R.drawable.edit);
        sliderPage1.setBgColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        sliderPage1 = new SliderPage();
        sliderPage1.setTitle("Cek Permohonanan");
        sliderPage1.setDescription("Tracking progres permohonan anda secara online");
        sliderPage1.setImageDrawable(R.drawable.search);
        sliderPage1.setBgColor(Color.parseColor("#d3d742"));
        addSlide(AppIntroFragment.newInstance(sliderPage1));

        doneButton.setVisibility(View.GONE);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
        intent.putExtra("first", true);
        startActivity(intent);
        finish();
    }
}
