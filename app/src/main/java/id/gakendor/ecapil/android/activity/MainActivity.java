package id.gakendor.ecapil.android.activity;

import android.app.LauncherActivity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.client.ApiUtils;
import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.model.User;
import id.gakendor.ecapil.android.util.IDRUtils;
import id.gakendor.ecapil.android.util.NotificationHelper;
import id.gakendor.ecapil.android.util.Preferences;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;

    BottomSheetBehavior sheetBehavior;
    @BindView(R.id.txt_toggle)
    TextView txtToggle;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_idr)
    TextView tvIdr;
    @BindView(R.id.tv_phone)
    TextView tvPhone;
    MobileService mobileService;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);
        ButterKnife.bind(this);
//        startActivity(new Intent(getApplicationContext(), FirebasePhoneNumAuthActivity.class));
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        mobileService = ApiUtils.MobileService(getApplicationContext());

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        txtToggle.setText("TUTUP");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        txtToggle.setText("BUKA");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        User user = Preferences.getUser(getApplicationContext());

        tvName.setText(user.getName());
        tvPhone.setText(user.getPhone());
        tvDate.setText("KURS USD to IDR today");
        loadKurs();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadKurs();
            }
        });
    }

    private void loadKurs() {
        tvIdr.setText("Loading...");
        mobileService.kursUSD_IDR("http://free.currencyconverterapi.com/api/v5/convert?q=USD_IDR&compact=y").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    try {
                        String json = response.body().string();
                        JsonParser parser = new JsonParser();
                        JsonObject o = parser.parse(json).getAsJsonObject();
                        tvIdr.setText(IDRUtils.toRupiah(o.get("USD_IDR").getAsJsonObject().get("val").getAsDouble()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvIdr.setText("Failed load, click to refresh");
                        tvIdr.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                loadKurs();
                            }
                        });
                    }
                } else {
                    tvIdr.setText("Failed load, click to refresh");
                    tvIdr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            loadKurs();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                tvIdr.setText("Failed load, click to refresh");
                tvIdr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadKurs();
                    }
                });
            }
        });
    }

    @OnClick(R.id.txt_toggle)
    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            txtToggle.setText("TUTUP");
        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            txtToggle.setText("BUKA");
        }
    }

    @OnClick({R.id.cv_booking, R.id.cv_my_booking, R.id.cv_history, R.id.cv_settings, R.id.cv_about, R.id.cv_logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_booking:
                startActivity(new Intent(getApplicationContext(), PilihTempatActivity.class));
                break;
            case R.id.cv_my_booking:
                startActivity(new Intent(getApplicationContext(), MyBookingActivity.class));
                break;
            case R.id.cv_history:
                break;
            case R.id.cv_settings:
                break;
            case R.id.cv_about:
                break;
            case R.id.cv_logout:
                new MaterialDialog.Builder(MainActivity.this)
                        .title("Apakah anda yakin akan logout?")
                        .positiveText("Ya")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Preferences.setUser(getApplicationContext(), null);
                                Intent intent = new Intent(getApplicationContext(), AuthActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        })
                        .negativeText("Tidak")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                            }
                        })
                        .show();

                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            txtToggle.setText("BUKA");
        } else{
            super.onBackPressed();
        }
    }
}
