package id.gakendor.ecapil.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.adapter.MyBookingAdapter;
import id.gakendor.ecapil.android.client.ApiUtils;
import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.client.model.Response;
import id.gakendor.ecapil.android.model.MyTrxBooking;
import id.gakendor.ecapil.android.model.Permohonan;
import id.gakendor.ecapil.android.model.User;
import id.gakendor.ecapil.android.util.BaseActivity;
import id.gakendor.ecapil.android.util.Preferences;
import id.gakendor.ecapil.android.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class MyBookingActivity extends BaseActivity {

    MobileService mobileService;
    User user;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_empty)
    CardView llEmpty;
    @BindView(R.id.tv_booking)
    TextView tvBooking;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        mobileService = ApiUtils.MobileService(getApplicationContext());
        user = Preferences.getUser(getApplicationContext());

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        tvBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PilihTempatActivity.class));
                finish();
            }
        });
    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> map = new HashMap<>();
        map.put("id_user", String.valueOf(user.getId()));

        mobileService.getMyPermohonan(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<Permohonan> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<Permohonan>>() {
                        }.getType());
                        if (listBody.size() > 0) {
                            MyBookingAdapter adapter = new MyBookingAdapter(listBody, new MyBookingAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(Permohonan model) {
                                    Intent intent = new Intent(getApplicationContext(), MyBookingDetailActivity.class);
                                    intent.putExtra(Permohonan.class.getName(), model);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(adapter);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            llEmpty.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    llEmpty.setVisibility(View.VISIBLE);
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
                t.printStackTrace();
                recyclerView.setVisibility(View.GONE);
                llEmpty.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }
}
