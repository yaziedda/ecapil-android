package id.gakendor.ecapil.android.activity;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import net.glxn.qrgen.android.QRCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.adapter.MyBookingDetailAdapter;
import id.gakendor.ecapil.android.client.ApiUtils;
import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.client.model.Response;
import id.gakendor.ecapil.android.model.MsStatus;
import id.gakendor.ecapil.android.model.MyTrxBooking;
import id.gakendor.ecapil.android.model.Permohonan;
import id.gakendor.ecapil.android.model.PermohonanDetail;
import id.gakendor.ecapil.android.util.BaseActivity;
import id.gakendor.ecapil.android.util.NotificationHelper;
import id.gakendor.ecapil.android.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class MyBookingDetailActivity extends BaseActivity {

    MyTrxBooking myTrxBooking;
    MobileService mobileService;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.ll_v)
    LinearLayout llV;
    @BindView(R.id.rl_not_v)
    LinearLayout rlNotV;
    @BindView(R.id.tv_bank)
    TextView tvBank;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_service)
    TextView tvService;
    @BindView(R.id.tv_service_desc)
    TextView tvServiceDesc;
    @BindView(R.id.tv_estimate)
    TextView tvEstimate;
    @BindView(R.id.tv_code)
    TextView tvCode;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.iv_scan)
    ImageView ivScan;
    @BindView(R.id.rl_scan_not_v)
    RelativeLayout rlScanNotV;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    Permohonan permohonan;
    List<MsStatus> msStatusList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_booking_detail);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        myTrxBooking = (MyTrxBooking) getIntent().getSerializableExtra("myTrxBooking");
        permohonan = (Permohonan) getIntent().getSerializableExtra(Permohonan.class.getName());
        mobileService = ApiUtils.MobileService(getApplicationContext());

        tvBank.setText("PERMOHONAN "+permohonan.getPermohonan());
        tvDate.setText(permohonan.getKedatangan());
        tvCode.setText("#"+permohonan.getKodePermohonan());
        tvService.setText(permohonan.getStatus());

//        checkVerify();
//
//        tvBank.setText(myTrxBooking.getBankName());
//        tvService.setText(myTrxBooking.getServiceName());
//        tvServiceDesc.setText(myTrxBooking.getServiceDescription());
//        tvCode.setText(myTrxBooking.getCodeBooking());
//        tvDate.setText(myTrxBooking.getDate());
//        tvEstimate.setText(myTrxBooking.getDiff().replaceAll("`", "'"));
//        tvTime.setText(myTrxBooking.getTime());

//        loadData();

//        ivScan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TedPermission.with(MyBookingDetailActivity.this)
//                        .setPermissionListener(new PermissionListener() {
//                            @Override
//                            public void onPermissionGranted() {
//                                Intent i = new Intent(getApplicationContext(), ScanActivity.class);
//                                i.putExtra("myTrxBooking", myTrxBooking);
//                                startActivityForResult(i, 1812);
//                            }
//
//                            @Override
//                            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
//                                showMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]");
//                            }
//                        })
//                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
//                        .setPermissions(Manifest.permission.CAMERA)
//                        .check();
//
//            }
//        });



        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadMaster();
            }
        });

//        if(myTrxBooking.getStatus() == 0){
//            tvCancel.setVisibility(View.VISIBLE);
//        }else{
//            tvCancel.setVisibility(View.GONE);
//        }
//
//        tvCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new MaterialDialog.Builder(MyBookingDetailActivity.this)
//                        .title("Konfirmasi")
//                        .content("Apakah anda yakin akan membatalkan antrian?")
//                        .positiveText("Ya")
//                        .negativeText("Tidak")
//                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                            @Override
//                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                showPleasewaitDialog();
//                                Map<String, String> map = new HashMap<>();
//                                map.put("id", String.valueOf(myTrxBooking.getId()));
//
//                                mobileService.cancel(map).enqueue(new Callback<Response>() {
//                                    @Override
//                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                                        dissmissPleasewaitDialog();
//                                        if(response.isSuccessful() && response.body().isState()){
//                                            showMessage("Sukses cancel");
//                                            NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
//                                            notificationHelper.cancelNotif();
//                                            loadData();
//                                        }else{
//                                            showMessage(Static.SOMETHING_WRONG);
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onFailure(Call<Response> call, Throwable t) {
//                                        dissmissPleasewaitDialog();
//                                        showMessage(Static.SOMETHING_WRONG);
//                                    }
//                                });
//                            }
//                        }).show();
//            }
//        });
        loadMaster();
        Bitmap myBitmap = QRCode.from(new Gson().toJson(permohonan)).bitmap();
        ivScan.setImageBitmap(myBitmap);
    }

    private void loadMaster() {
        msStatusList.clear();
        showPleasewaitDialog();
        mobileService.getStatus().enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                dissmissPleasewaitDialog();
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<MsStatus> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MsStatus>>() {
                        }.getType());
                        msStatusList.addAll(listBody);
                        loadData();
                    }else{
                        showMessage(Static.SOMETHING_WRONG);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                dissmissPleasewaitDialog();
                showMessage(Static.SOMETHING_WRONG);
            }
        });
    }

    private void loadData() {
//        showPleasewaitDialog();
        swipeRefreshLayout.setRefreshing(true);

        Map<String, String> map = new HashMap<>();
        map.put("id_trx", String.valueOf(permohonan.getId()));
//        map.put("bank_id", String.valueOf(myTrxBooking.getBankId()));
//        map.put("service_id", String.valueOf(myTrxBooking.getServiceId()));
//        map.put("date", myTrxBooking.getDateId());

        mobileService.getMyPermohonanById(map).enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful()) {
                    Response body = response.body();
                    if (body.getData() != null) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                        List<PermohonanDetail> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<PermohonanDetail>>() {
                        }.getType());

                        for (int i=0; i < msStatusList.size(); i++){
                            MsStatus msStatus = msStatusList.get(i);

                            for (int z=0; z < listBody.size(); z++){
                                PermohonanDetail permohonanDetail = listBody.get(z);
                                if(msStatus.getId() == permohonanDetail.getIdStatus()){
                                    msStatus.setExist(true);
                                    msStatusList.set(i, msStatus);
                                    break;
                                }
                            }
                        }

                        MyBookingDetailAdapter adapter = new MyBookingDetailAdapter(msStatusList, new MyBookingDetailAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(MsStatus model) {

                            }
                        });
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
                t.printStackTrace();
            }
        });
    }

    private void checkVerify() {
        if (myTrxBooking.getArrived() == 0) {
            rlNotV.setVisibility(View.VISIBLE);
            rlScanNotV.setVisibility(View.VISIBLE);
            llV.setVisibility(View.GONE);
        } else {
            rlNotV.setVisibility(View.GONE);
            rlScanNotV.setVisibility(View.GONE);
            llV.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1812) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                Map<String, String> map = new HashMap<>();
                map.put("user_id", String.valueOf(myTrxBooking.getUserId()));
                map.put("bank_id", String.valueOf(myTrxBooking.getBankId()));
                map.put("service_id", String.valueOf(myTrxBooking.getServiceId()));
                map.put("date", myTrxBooking.getDateId());
                map.put("tr_id", String.valueOf(myTrxBooking.getId()));
                map.put("qr_id", result);
                showPleasewaitDialog();
                mobileService.bookingVerify(map).enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        if (response.isSuccessful()) {
                            if (response.body().isState()) {
                                MyTrxBooking model = new Gson().fromJson(new Gson().toJson(response.body().getData()), MyTrxBooking.class);
                                myTrxBooking.setArrived(model.getArrived());
                                checkVerify();
                                showMessage("Sukses verifikasi");
                            } else {
                                showMessage("Gagal verifikasi");
                            }
                        } else {
                            showMessage(Static.SOMETHING_WRONG);
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage(Static.SOMETHING_WRONG);
                    }
                });
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
//                showMessage("Barcode tidak valid");
            }
        }
    }//onActivityResult

    private boolean checkQrFormat(String text) {
        try {
            String[] data = text.split("-");
            if (Integer.valueOf(data[0]) != myTrxBooking.getBankId()) {
                return false;
            }
            if (Integer.valueOf(data[1]) != myTrxBooking.getServiceId()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }


}
