package id.gakendor.ecapil.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.adapter.SyaratImageAdapter;
import id.gakendor.ecapil.android.client.ApiUtils;
import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.client.model.Response;
import id.gakendor.ecapil.android.model.MsPermohonan;
import id.gakendor.ecapil.android.model.MsPermohonanSyarat;
import id.gakendor.ecapil.android.model.TrPermohonan;
import id.gakendor.ecapil.android.util.BaseActivity;
import id.gakendor.ecapil.android.util.PartUtils;
import id.gakendor.ecapil.android.util.Preferences;
import id.gakendor.ecapil.android.util.Static;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;

public class PertanyaanActivity extends BaseActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_1)
    TextView tv1;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    MobileService mobileService;
    MsPermohonan msPermohonan;
    public static Activity fa;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;
    Uri inputUri, outputUri;
    @BindView(R.id.bt_upload)
    Button btUpload;
    private List<MsPermohonanSyarat> listImageSyarat = new ArrayList<>();
    private MsPermohonanSyarat imageSelected;
    int count = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertanyaan);
        ButterKnife.bind(this);
        tvTitle.setText("PERSYARATAN");

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        fa = this;

        msPermohonan = (MsPermohonan) getIntent().getSerializableExtra(MsPermohonan.class.getName());

        tvTitle.setText("PERSYARATAN " + msPermohonan.getName());
        mobileService = ApiUtils.MobileService(getApplicationContext());

        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        btUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int full = 0;
                for (MsPermohonanSyarat msPermohonanSyarat:listImageSyarat){
                    if(msPermohonanSyarat.getPath() != null){
                        full = full + 1;
                    }
                }

                if(full == listImageSyarat.size()){
                    requestPermohonan();
                }else{
                    showMessage("Upload semua persyaratan !");
                }
            }
        });
    }

    private void requestPermohonan() {
        showPleasewaitDialog();
        final Map<String, String> map = new HashMap<>();
        map.put("id_user", String.valueOf(Preferences.getUser(getApplicationContext()).getId()));
        map.put("id_permohonan", String.valueOf(msPermohonan.getId()));
        mobileService.addPermohonan(map)
                .enqueue(new Callback<Response>() {
                    @Override
                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                        dissmissPleasewaitDialog();
                        Response body = response.body();
                        if(body != null){
                            Gson gson = new Gson();
                            JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                            JsonElement permohonan = jsonObject.get("data").getAsJsonObject().get("permohonan");
                            JsonElement tr_permohonan = jsonObject.get("data").getAsJsonObject().get("tr_permohonan");

                            final MsPermohonan msPermohonan = gson.fromJson(permohonan, MsPermohonan.class);
                            final TrPermohonan trPermohonan = gson.fromJson(tr_permohonan, TrPermohonan.class);

                            System.out.println(gson.toJson(msPermohonan));
                            System.out.println(gson.toJson(trPermohonan));

                            count = 1;
                            for (MsPermohonanSyarat model :
                                    listImageSyarat) {
                                showPleasewaitDialog();
//                                showMessage("Uploading "+count+"/"+listImageSyarat.size());

                                RequestBody id_permohonan = PartUtils.createPartFromString(String.valueOf(trPermohonan.getId()));
                                RequestBody id_permohonan_syarat = PartUtils.createPartFromString(String.valueOf(String.valueOf(model.getId())));

                                Map<String, RequestBody> map2 = new HashMap<>();
                                map2.put("id_permohonan", id_permohonan);
                                map2.put("id_permohonan_syarat", id_permohonan_syarat);

                                mobileService.addPermohonanUpload(map2, model.getBody()).enqueue(new Callback<Response>() {
                                    @Override
                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
                                        count++;
                                        if(count >= listImageSyarat.size()){
//                                            showMessage("Upload success ");
//                                            dissmissPleasewaitDialog();
//                                            Intent intent = new Intent(getApplicationContext(), SumaryActivity.class);
//                                            intent.putExtra(MsPermohonan.class.getName(), msPermohonan);
//                                            intent.putExtra(TrPermohonan.class.getName(), trPermohonan);
//                                            startActivity(intent);
//                                            finish();
                                        }

                                        System.out.println(new Gson().toJson(response.body()));
                                    }

                                    @Override
                                    public void onFailure(Call<Response> call, Throwable t) {
                                        System.out.println(t.getMessage());
                                        count++;
                                        if(count >= listImageSyarat.size()){
//                                            showMessage("Upload success");
//                                            dissmissPleasewaitDialog();
//                                            Intent intent = new Intent(getApplicationContext(), SumaryActivity.class);
//                                            intent.putExtra(MsPermohonan.class.getName(), msPermohonan);
//                                            intent.putExtra(TrPermohonan.class.getName(), trPermohonan);
//                                            startActivity(intent);
//                                            finish();
                                        }
                                    }
                                });
                            }

                            showMessage("Upload success");
                            dissmissPleasewaitDialog();
                            Intent intent = new Intent(getApplicationContext(), SumaryActivity.class);
                            intent.putExtra(MsPermohonan.class.getName(), msPermohonan);
                            intent.putExtra(TrPermohonan.class.getName(), trPermohonan);
                            startActivity(intent);
                            finish();



                        }else{
                            showMessage("Failed to request permohonan");
                        }
                    }

                    @Override
                    public void onFailure(Call<Response> call, Throwable t) {
                        dissmissPleasewaitDialog();
                        showMessage("Failed to request permohonan");
                    }
                });
    }

    private void loadData() {
//        showPleasewaitDialog();
        listImageSyarat.clear();
        swipeRefreshLayout.setRefreshing(true);
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(msPermohonan.getId()));
        mobileService.getPermohonanSyarat(map).enqueue(new Callback<Response>() {
                            @Override
                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                dissmissPleasewaitDialog();
                                swipeRefreshLayout.setRefreshing(false);
                                if (response.isSuccessful()) {
                                    Response body = response.body();
                                    if (body.getData() != null) {
                                        Gson gson = new Gson();
                                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
                                        List<MsPermohonanSyarat> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MsPermohonanSyarat>>() {
                                        }.getType());

                                        listImageSyarat.addAll(listBody);
//                        ServiceAdapter adapter = new ServiceAdapter(listBody, new ServiceAdapter.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(Service model) {
//                                Intent intent = new Intent(getApplicationContext(), SumaryActivity.class);
//                                intent.putExtra("msPermohonan", msPermohonan);
//                                intent.putExtra("service", model);
//                                startActivity(intent);
//                            }
//                        });
//                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//                        recyclerView.setLayoutManager(layoutManager);
//                        recyclerView.setAdapter(adapter);
                                        loadImageAdapter();
                    }
                } else {
                    showMessage(Static.SOMETHING_WRONG);
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
//                dissmissPleasewaitDialog();
                swipeRefreshLayout.setRefreshing(false);
                showMessage(Static.SOMETHING_WRONG);
            }
        });
    }

    private void loadImageAdapter() {
        SyaratImageAdapter imageAdapter = new SyaratImageAdapter(PertanyaanActivity.this, listImageSyarat, new SyaratImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final MsPermohonanSyarat model) {
                toPick(model);
            }
        });
        recyclerView.setLayoutManager(new GridLayoutManager(PertanyaanActivity.this, 2));
        recyclerView.setAdapter(imageAdapter);
    }

    @OnClick(R.id.iv_finish)
    public void onViewClicked() {
        finish();
    }

    private void doCompres() {
        try {
            File file = new Compressor(PertanyaanActivity.this)
                    .setMaxWidth(1280)
                    .setMaxHeight(720)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(new File(outputUri.getPath()));
            doUpload(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Crop.REQUEST_CROP) {

            doCompres();
        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {
                if (source == EasyImage.ImageSource.CAMERA) {
                    File photoFile = EasyImage.lastlyTakenButCanceledPhoto(PertanyaanActivity.this);
                    if (photoFile != null) photoFile.delete();
                    Toast.makeText(getApplicationContext(), "Upload canceled", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                Log.d("error", e.getMessage());
                Toast.makeText(getApplicationContext(), "Failed to upload cause something wrong, please try again", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onImagePicked(final File imageFile, EasyImage.ImageSource source, int type) {
                try {
                    inputUri = Uri.fromFile(imageFile);
                    outputUri = Uri.fromFile(new File(getCacheDir(), String.valueOf(System.currentTimeMillis() % 1000)));
                    Crop.of(inputUri, outputUri).start(PertanyaanActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void doUpload(File file) {
//        RequestBody reqFile = RequestBody.create(MediaType.parse("*"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        MultipartBody.Part body = PartUtils.prepareFilePart("gambar", file);

        for (int i = 0; i < listImageSyarat.size(); i++) {
            MsPermohonanSyarat msPermohonanSyarat1 = listImageSyarat.get(i);
            if (msPermohonanSyarat1.getId() == imageSelected.getId()) {
                msPermohonanSyarat1.setPath(file.getPath());
                msPermohonanSyarat1.setBody(body);
                listImageSyarat.set(i, msPermohonanSyarat1);
                loadImageAdapter();
                break;
            }
        }
//        presenter.uploadPhoto(body, priority_toched);
    }

    public void showPick() {
        new MaterialDialog.Builder(PertanyaanActivity.this)
                .items(new String[]{"CAMERA", "GALERY"})
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        if (position == 0) {
                            EasyImage.openCamera(PertanyaanActivity.this, 1231);
                        } else if (position == 1) {
                            EasyImage.openGallery(PertanyaanActivity.this, 1232);
                        }
                    }
                }).show();
    }

    public void setImageListAdapter(final List<MsPermohonanSyarat> listAdapter) {

    }

    public void toShowImage(MsPermohonanSyarat model) {
//        Intent intent = new Intent(getApplicationContext(), ShowImageMembersActivity.class);
//        intent.putExtra(MsPermohonanSyarat.class.getName(), model);
//        startActivity(intent);
    }

    public void toPick(final MsPermohonanSyarat model) {
        imageSelected = model;
        TedPermission.with(PertanyaanActivity.this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        showPick();
                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        finish();
                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA)
                .check();
    }
}
