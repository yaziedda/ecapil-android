package id.gakendor.ecapil.android.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.glxn.qrgen.android.QRCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.adapter.MyBookingDetailAdapter;
import id.gakendor.ecapil.android.client.ApiUtils;
import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.client.model.Response;
import id.gakendor.ecapil.android.model.MsPermohonan;
import id.gakendor.ecapil.android.model.MyTrxBooking;
import id.gakendor.ecapil.android.model.Place;
import id.gakendor.ecapil.android.model.Service;
import id.gakendor.ecapil.android.model.TrPermohonan;
import id.gakendor.ecapil.android.model.TrxBooking;
import id.gakendor.ecapil.android.model.User;
import id.gakendor.ecapil.android.util.BaseActivity;
import id.gakendor.ecapil.android.util.NotificationHelper;
import id.gakendor.ecapil.android.util.Preferences;
import id.gakendor.ecapil.android.util.Static;
import retrofit2.Call;
import retrofit2.Callback;

public class SumaryActivity extends BaseActivity {

    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_bank)
    TextView tvBank;
    @BindView(R.id.tv_service)
    TextView tvService;
    @BindView(R.id.tv_service_desc)
    TextView tvServiceDesc;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.tv_time)
    TextView tvTime;
    MobileService mobileService;
    Place place;
    Service service;
    User user;
    @BindView(R.id.bt_cancel)
    TextView btCancel;
    @BindView(R.id.bt_confirm)
    TextView btConfirm;
    @BindView(R.id.recycleview)
    RecyclerView recyclerView;
    @BindView(R.id.container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_kedatangan)
    TextView tvKedatangan;
    @BindView(R.id.tv_id_permohonan)
    Button tvIdPermohonan;
    @BindView(R.id.bt_selesai)
    Button btSelesai;
    MsPermohonan msPermohonan;
    TrPermohonan trPermohonan;
    @BindView(R.id.tv_qr)
    ImageView tvQr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sumary);
        ButterKnife.bind(this);

        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        place = (Place) getIntent().getSerializableExtra("msPermohonan");
        service = (Service) getIntent().getSerializableExtra("service");
        trPermohonan = (TrPermohonan) getIntent().getSerializableExtra(TrPermohonan.class.getName());
        msPermohonan = (MsPermohonan) getIntent().getSerializableExtra(MsPermohonan.class.getName());
        user = Preferences.getUser(getApplicationContext());

        tvName.setText("");
        tvBank.setText("");
        tvService.setText("");
        tvServiceDesc.setText("");
        tvDay.setText("");
        tvTime.setText("");

        mobileService = ApiUtils.MobileService(getApplicationContext());
        recyclerView.setNestedScrollingEnabled(false);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        loadData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });

        tvKedatangan.setText("TANGGAL : " + trPermohonan.getKedatangan());
        tvIdPermohonan.setText("#" + trPermohonan.getKodePermohonan());

        Bitmap myBitmap = QRCode.from(new Gson().toJson(trPermohonan)).bitmap();
        tvQr.setImageBitmap(myBitmap);

        btSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



    }

    private void loadData() {
//        final Map<String, String> map = new HashMap<>();
//        map.put("user_id", String.valueOf(user.getId()));
//        map.put("bank_id", String.valueOf(place.getId()));
//        map.put("service_id", String.valueOf(service.getId()));
//
////        showPleasewaitDialog();
//        swipeRefreshLayout.setRefreshing(true);
//        mobileService.booking(map).enqueue(new Callback<Response>() {
//            @Override
//            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                if (response.isSuccessful()) {
//                    Response body = response.body();
//                    if (body.isState() && body.getData() != null) {
//                        TrxBooking trxBooking = new Gson().fromJson(new Gson().toJson(body.getData()), TrxBooking.class);
//                        tvName.setText(user.getName());
//                        tvBank.setText(trxBooking.getPlace().getName());
//                        tvService.setText(trxBooking.getService().getName());
//                        tvServiceDesc.setText(trxBooking.getService().getDescription());
//                        tvDay.setText(trxBooking.getDate());
//                        tvTime.setText(trxBooking.getTime());
//
//                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//                        Date today = Calendar.getInstance().getTime();
//                        String date = df.format(today);
//
//                        map.put("date", date);
//
//                        mobileService.getMyBookingDetail(map).enqueue(new Callback<Response>() {
//                            @Override
//                            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
////                                dissmissPleasewaitDialog();
//                                swipeRefreshLayout.setRefreshing(false);
//                                if (response.isSuccessful()) {
//                                    Response body = response.body();
//                                    if (body.getData() != null) {
//                                        Gson gson = new Gson();
//                                        JsonObject jsonObject = gson.toJsonTree(body).getAsJsonObject();
//                                        List<MyTrxBooking> listBody = gson.fromJson(jsonObject.getAsJsonArray("data"), new TypeToken<List<MyTrxBooking>>() {
//                                        }.getType());
//                                        MyBookingDetailAdapter adapter = new MyBookingDetailAdapter(listBody, new MyBookingDetailAdapter.OnItemClickListener() {
//                                            @Override
//                                            public void onItemClick(MyTrxBooking model) {
//                                            }
//                                        });
//                                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//                                        recyclerView.setLayoutManager(layoutManager);
//                                        recyclerView.setAdapter(adapter);
//                                    }
//                                } else {
//                                    showMessage(Static.SOMETHING_WRONG);
//                                }
//                            }
//
//                            @Override
//                            public void onFailure(Call<Response> call, Throwable t) {
////                                dissmissPleasewaitDialog();
//                                swipeRefreshLayout.setRefreshing(false);
//                                showMessage(Static.SOMETHING_WRONG);
//                                t.printStackTrace();
//                            }
//                        });
//
//                        btConfirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                new MaterialDialog.Builder(SumaryActivity.this)
//                                        .title("Konfirmasi")
//                                        .content("Apakah anda yakin akan melakukan antrian?")
//                                        .positiveText("Ya")
//                                        .negativeText("Tidak")
//                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
//                                            @Override
//                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                                                showPleasewaitDialog();
//                                                mobileService.bookingConfirm(map).enqueue(new Callback<Response>() {
//                                                    @Override
//                                                    public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {
//                                                        dissmissPleasewaitDialog();
//                                                        if (response.isSuccessful()) {
//                                                            Response body = response.body();
//                                                            if (body.isState()) {
//                                                                showMessage("Sukses Boking Antrian");
//                                                                PilihTempatActivity.fa.finish();
//                                                                PertanyaanActivity.fa.finish();
//                                                                NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
//                                                                notificationHelper.createNotification("E-CAPIL", "ANTRIAN ANDA SEDANG BERLANGUNG");
//
//                                                                finish();
//                                                                startActivity(new Intent(getApplicationContext(), MyBookingActivity.class));
//                                                            } else {
//                                                                showMessage(Static.SOMETHING_WRONG);
//                                                                finish();
//                                                            }
//                                                        } else {
//                                                            showMessage(Static.SOMETHING_WRONG);
//                                                            finish();
//                                                        }
//                                                    }
//
//                                                    @Override
//                                                    public void onFailure(Call<Response> call, Throwable t) {
//                                                        dissmissPleasewaitDialog();
//                                                        showMessage(Static.SOMETHING_WRONG);
//                                                        finish();
//                                                    }
//                                                });
//                                            }
//                                        }).show();
//
//                            }
//                        });
//                    } else {
//                        Double code;
//                        try {
//                            code = (Double) body.getData();
//                        } catch (Exception e) {
//                            code = 0.0;
//                        }
//
//                        switch (code.intValue()) {
//                            case 1:
//                                showMessage("Anda masih memiliki antrian");
//                                PilihTempatActivity.fa.finish();
//                                PertanyaanActivity.fa.finish();
//                                finish();
//                                startActivity(new Intent(getApplicationContext(), MyBookingActivity.class));
//                                break;
//                            case 2:
//                                showMessage("Maaf, antrian penuh");
//                                break;
//                            case 3:
//                                showMessage("Maaf, layanan telah tutup");
//                                break;
//                            default:
//                                showMessage("Maaf, antrian tidak dapat di akses");
//                                break;
//                        }
//                        finish();
//                    }
//                } else {
//                    showMessage(Static.SOMETHING_WRONG);
//                    finish();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Response> call, Throwable t) {
////                dissmissPleasewaitDialog();
//                swipeRefreshLayout.setRefreshing(false);
//                showMessage(Static.SOMETHING_WRONG);
//                finish();
//            }
//        });
    }

    private void sendMyNotification() {

        //On click of notification it redirect to this Activity
        Intent intent = new Intent(this, MyBookingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Antrian sedang berjalan")
                .setContentText("Klik untuk monitor antrian anda")
                .setOngoing(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
