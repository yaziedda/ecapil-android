package id.gakendor.ecapil.android.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.model.MsPermohonan;
import id.gakendor.ecapil.android.model.Place;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder> {

    List<MsPermohonan> list = new ArrayList<>();


    public interface OnItemClickListener {
        void onItemClick(MsPermohonan model);
    }

    private final OnItemClickListener listener;

    public BankAdapter(List<MsPermohonan> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MsPermohonan model = list.get(position);

        holder.tvName.setText(model.getName());
        holder.tvAddress.setText(model.getDesc());
        holder.btChoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.bt_choice)
        TextView btChoice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
