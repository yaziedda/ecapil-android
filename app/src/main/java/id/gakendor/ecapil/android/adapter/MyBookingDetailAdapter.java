package id.gakendor.ecapil.android.adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.model.MsStatus;
import id.gakendor.ecapil.android.model.MyTrxBooking;
import id.gakendor.ecapil.android.model.PermohonanDetail;
import id.gakendor.ecapil.android.model.User;
import id.gakendor.ecapil.android.util.Preferences;

public class MyBookingDetailAdapter extends RecyclerView.Adapter<MyBookingDetailAdapter.ViewHolder> {

    List<MsStatus> list = new ArrayList<>();


    public interface OnItemClickListener {
        void onItemClick(MsStatus model);
    }

    private final OnItemClickListener listener;

    public MyBookingDetailAdapter(List<MsStatus> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_booking_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MsStatus model = list.get(position);

        User user = Preferences.getUser(holder.itemView.getContext());
        if(model.isExist()){
            holder.tvCode.setText(model.getName());
            holder.tvTime.setText(model.getCreatedAt());
            holder.llBg.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.green_500));
        }else{
            holder.tvCode.setText(model.getName());
            holder.tvTime.setVisibility(View.GONE);
            holder.llBg.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.red_500));
        }

//        holder.tvService.setText(model.getServiceName());
//        holder.tvServiceDesc.setText(model.getServiceDescription());
//        holder.tvEstimate.setText(model.getDiff().replaceAll("`", "'"));
//        holder.tvCode.setText(model.getCodeBooking());
//        holder.tvTime.setText(model.getTime());
//        if(model.getStatus() == 1){
//            holder.tvService.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvServiceDesc.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvEstimate.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvCode.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvTime.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvState.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
//            holder.tvState.setBackgroundColor(Color.parseColor("#ffffff"));
//            if(model.getCancel() == 1){
//                holder.tvState.setText("CANCEL");
//            }else{
//                holder.tvState.setText("SELESAI");
//            }
//            holder.llBg.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
//            holder.tvEstimate.setVisibility(View.GONE);
//        }else if(model.getStatus() == 2){
//            holder.tvService.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvServiceDesc.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvEstimate.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvCode.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvTime.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvState.setTextColor(Color.parseColor("#ffbb34"));
//            holder.tvState.setBackgroundColor(Color.parseColor("#ffffff"));
//            holder.tvState.setText("SEDANG DALAM LAYANAN");
//            holder.llBg.setBackgroundColor(Color.parseColor("#ffbb34"));
//            holder.tvEstimate.setVisibility(View.GONE);
//        }else if(model.getStatus() == 3){
//            holder.tvService.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvServiceDesc.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvEstimate.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvCode.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvTime.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvState.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
//            holder.tvState.setBackgroundColor(Color.parseColor("#ffffff"));
//            holder.tvState.setText("TERLEWAT");
//            holder.llBg.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_red_dark));
//            holder.tvEstimate.setVisibility(View.GONE);
//        }else if(model.getUserId() == user.getId()){
//            holder.tvEstimate.setVisibility(View.VISIBLE);
//            holder.tvService.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvServiceDesc.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvEstimate.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvCode.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvTime.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvState.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));
//            holder.tvState.setBackgroundColor(Color.parseColor("#ffffff"));
//            holder.tvState.setText("MENUNGGU ( ANTRIAN ANDA )");
//            holder.llBg.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));
//        }else {
//            holder.tvEstimate.setVisibility(View.VISIBLE);
//            holder.tvService.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvServiceDesc.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvEstimate.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvCode.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvTime.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvTime.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvState.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), android.R.color.holo_green_dark));
//            holder.tvState.setTextColor(Color.parseColor("#ffffff"));
//            holder.tvState.setText("MENUNGGU");
//            holder.llBg.setBackgroundColor(Color.parseColor("#ffffff"));
//        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_service)
        TextView tvService;
        @BindView(R.id.tv_service_desc)
        TextView tvServiceDesc;
        @BindView(R.id.tv_estimate)
        TextView tvEstimate;
        @BindView(R.id.tv_code)
        TextView tvCode;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_state)
        TextView tvState;
        @BindView(R.id.ll_bg)
        LinearLayout llBg;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
