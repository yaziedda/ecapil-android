package id.gakendor.ecapil.android.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import id.gakendor.ecapil.android.R;
import id.gakendor.ecapil.android.model.MsPermohonanSyarat;

public class SyaratImageAdapter extends RecyclerView.Adapter<SyaratImageAdapter.SyaratImageAdapterHolder> {

    private Activity activity;
    private List<MsPermohonanSyarat> matchReceiverList = new ArrayList<>();
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(MsPermohonanSyarat model);
    }

    public SyaratImageAdapter(Activity activity, List<MsPermohonanSyarat> list, OnItemClickListener listener) {
        this.activity = activity;
        this.matchReceiverList = list;
        this.listener = listener;
    }

    @Override
    public SyaratImageAdapterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_edit_profile_image, parent, false);
        SyaratImageAdapterHolder matchListyHolder = new SyaratImageAdapterHolder(itemView);
        return matchListyHolder;
    }

    @Override
    public void onBindViewHolder(SyaratImageAdapterHolder holder, int position) {
        final MsPermohonanSyarat item = matchReceiverList.get(position);
        holder.itemView.setTag(position);

        if(item.getPath() != null){
            Glide.with(holder.itemView.getContext())
                    .load(new File(item.getPath())).into(holder.imgUser);
        }

        holder.tvName.setText(item.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return (matchReceiverList != null) ? matchReceiverList.size() : 0;
    }

    public static class SyaratImageAdapterHolder extends RecyclerView.ViewHolder {

        private ImageView imgUser;
        private TextView tvName;

        public SyaratImageAdapterHolder(View itemView) {
            super(itemView);
            imgUser = itemView.findViewById(R.id.imgProfile);
            tvName = itemView.findViewById(R.id.tv_name);
        }
    }

}
