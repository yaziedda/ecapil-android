package id.gakendor.ecapil.android.client;

import android.content.Context;

import id.gakendor.ecapil.android.client.mobile.MobileService;
import id.gakendor.ecapil.android.util.Static;


public class ApiUtils {

    public static String API = Static.BASE_URL;

    public static MobileService MobileService(Context context){
        return RetrofitClient.getClient(context, API).create(MobileService.class);
    }


}
