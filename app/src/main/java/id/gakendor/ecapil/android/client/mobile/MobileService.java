package id.gakendor.ecapil.android.client.mobile;


import java.util.Map;


import id.gakendor.ecapil.android.client.model.Response;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface MobileService {

    @FormUrlEncoded
    @POST("auth")
    Call<Response> auth(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("reg")
    Call<Response> reg(@FieldMap Map<String, String> map);

    @GET("ms-permohonan")
    Call<Response> getPermohonan();

    @GET("ms-permohonan-syarat")
    Call<Response> getPermohonanSyarat(@QueryMap Map<String, String> map);

    @GET("ms-status")
    Call<Response> getStatus();

    @FormUrlEncoded
    @POST("tr-permohonan")
    Call<Response> addPermohonan(@FieldMap Map<String, String> map);

    @Multipart
    @POST("image/upload")
    Call<Response> addPermohonanUpload(@PartMap Map<String, RequestBody> map,
                                           @Part MultipartBody.Part file);

    @GET("place")
    Call<Response> getPlace();

    @GET("service")
    Call<Response> getService(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("booking")
    Call<Response> booking(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("booking-confirm")
    Call<Response> bookingConfirm(@FieldMap Map<String, String> map);

    @GET("my-booking")
    Call<Response> getMyBooking(@QueryMap Map<String, String> map);

    @GET("my-booking-detail")
    Call<Response> getMyBookingDetail(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("verify")
    Call<Response> bookingVerify(@FieldMap Map<String, String> map);

    @GET
    public Call<ResponseBody> kursUSD_IDR(@Url String url);

    @FormUrlEncoded
    @POST("cancel")
    Call<Response> cancel(@FieldMap Map<String, String> map);

    @GET("my-permohonan")
    Call<Response> getMyPermohonan(@QueryMap Map<String, String> map);

    @GET("my-permohonan-detail")
    Call<Response> getMyPermohonanById(@QueryMap Map<String, String> map);

}
