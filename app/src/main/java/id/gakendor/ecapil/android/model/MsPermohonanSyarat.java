package id.gakendor.ecapil.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.MultipartBody;

public class MsPermohonanSyarat {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("id_permohonan")
    @Expose
    private Integer idPermohonan;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("is_upload")
    @Expose
    private Integer isUpload;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("value")
    @Expose
    private String path;
    @SerializedName("body")
    @Expose
    private MultipartBody.Part body;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdPermohonan() {
        return idPermohonan;
    }

    public void setIdPermohonan(Integer idPermohonan) {
        this.idPermohonan = idPermohonan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(Integer isUpload) {
        this.isUpload = isUpload;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MultipartBody.Part getBody() {
        return body;
    }

    public void setBody(MultipartBody.Part body) {
        this.body = body;
    }
}
