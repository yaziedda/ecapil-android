package id.gakendor.ecapil.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Permohonan implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("kode_permohonan")
    @Expose
    private String kodePermohonan;
    @SerializedName("id_user")
    @Expose
    private Integer idUser;
    @SerializedName("id_permohonan")
    @Expose
    private Integer idPermohonan;
    @SerializedName("name_user")
    @Expose
    private String nameUser;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("permohonan")
    @Expose
    private String permohonan;
    @SerializedName("kedatangan")
    @Expose
    private String kedatangan;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKodePermohonan() {
        return kodePermohonan;
    }

    public void setKodePermohonan(String kodePermohonan) {
        this.kodePermohonan = kodePermohonan;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdPermohonan() {
        return idPermohonan;
    }

    public void setIdPermohonan(Integer idPermohonan) {
        this.idPermohonan = idPermohonan;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPermohonan() {
        return permohonan;
    }

    public void setPermohonan(String permohonan) {
        this.permohonan = permohonan;
    }

    public String getKedatangan() {
        return kedatangan;
    }

    public void setKedatangan(String kedatangan) {
        this.kedatangan = kedatangan;
    }
}
