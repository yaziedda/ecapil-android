package id.gakendor.ecapil.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PermohonanDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id_trx")
    @Expose
    private Integer idTrx;
    @SerializedName("id_status")
    @Expose
    private Integer idStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    private boolean isExist;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdTrx() {
        return idTrx;
    }

    public void setIdTrx(Integer idTrx) {
        this.idTrx = idTrx;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isExist() {
        return isExist;
    }

    public void setExist(boolean exist) {
        isExist = exist;
    }
}
