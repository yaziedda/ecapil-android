package id.gakendor.ecapil.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TrPermohonan implements Serializable {

    @SerializedName("kode_permohonan")
    @Expose
    private String kodePermohonan;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_permohonan")
    @Expose
    private String idPermohonan;
    @SerializedName("kedatangan")
    @Expose
    private String kedatangan;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getKodePermohonan() {
        return kodePermohonan;
    }

    public void setKodePermohonan(String kodePermohonan) {
        this.kodePermohonan = kodePermohonan;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getIdPermohonan() {
        return idPermohonan;
    }

    public void setIdPermohonan(String idPermohonan) {
        this.idPermohonan = idPermohonan;
    }

    public String getKedatangan() {
        return kedatangan;
    }

    public void setKedatangan(String kedatangan) {
        this.kedatangan = kedatangan;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
