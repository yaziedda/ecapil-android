package id.gakendor.ecapil.android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrxBooking {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("bank")
    @Expose
    private Place place;
    @SerializedName("service")
    @Expose
    private Service service;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
